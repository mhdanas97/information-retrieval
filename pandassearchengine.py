import string
from collections import defaultdict
from datetime import datetime
from pathlib import Path
from typing import Tuple

import nltk
import numpy as np
import pandas as pd
from spellchecker import SpellChecker

import preprocessing


class PandasSearchEngine:
    """
    This class represents an instance of a search engine, built using Pandas
    """

    def __init__(self, normalized=True):
        self.normalized = normalized
        self.STOP_WORDS_LIST = frozenset(open('stop-words.txt').read().lower().split())
        self.bow_dataframe = self._get_bow_dataframe()
        self.idf_series = self._get_idf_series()
        self.tfidf_data_frame = self._get_tfidf_dataframe()

    def _get_bow_dataframe(self) -> pd.DataFrame:
        """
        Load saved bag of words dataframe
        or read documents in the corpus and calculate term frequencies

        PARAMETERS
        ----------
        normalized: Whether to normalize values by document length

        RETURNS
        -------
        Dataframe of term frequencies for each document in the corpus

        """

        bow_path = 'saved_models/pandas_bow{}.pkl'.format('_normalized' if self.normalized else '')
        if Path(bow_path).exists():
            return pd.read_pickle(bow_path)

        df = pd.DataFrame.from_dict(self._calculate_frequency()).fillna(0)
        df.to_pickle(bow_path)
        return df

    def _get_tfidf_dataframe(self) -> pd.DataFrame:
        """
        Load saved TF-IDF dataframe
        or read bag of words dataframe and calculate TF-IDF

        PARAMETERS
        ----------
        normalized: Normalize bow of words by document length

        RETURNS
        -------
        Dataframe of TF-IDF for each document in the corpus
        """
        tfidf_path = 'saved_models/pandas_tfidf{}.pkl'.format('_normalized' if self.normalized else '')
        if Path(tfidf_path).exists():
            return pd.read_pickle(tfidf_path)
        tfidf_data_frame = self.bow_dataframe.mul(self.idf_series, axis=0)
        tfidf_data_frame.to_pickle(tfidf_path)
        return tfidf_data_frame

    def _get_idf_series(self) -> pd.Series:
        return np.log2(423 / ((self.bow_dataframe > 0).sum(axis=1)))

    def _calculate_frequency(self) -> defaultdict:
        frequency = defaultdict(lambda: defaultdict(float))
        for f in range(1, 424):
            document = open('corpus/{}.txt'.format(f)).read().lower()

            document_tokens = [token.translate(str.maketrans('', '', string.punctuation)).strip()
                               for token in nltk.word_tokenize(document)
                               if token not in self.STOP_WORDS_LIST]

            document_length = len(document_tokens)
            for d in self._extract_dates(document):
                frequency[f][d] += 1 / (document_length if self.normalized else 1)

            for token in document_tokens:
                if bool(token):
                    frequency[f][token] += 1 / (
                        document_length if self.normalized else 1)

            for bigram in ('{} {}'.format(first, second)
                           for first, second in nltk.bigrams(document_tokens) if
                           bool(first) and bool(second)):
                frequency[f][bigram] += 1 / (document_length if self.normalized else 1)

        return frequency

    def _extract_dates(self, document: str):
        for match in preprocessing.DATES_RE.finditer(document):
            day, month, year = match.group('dayofmonth'), match.group('month')[:3], match.group('year')
            if len(day) > 2:
                year = day
                day = 1
            yield str(datetime.strptime('{}/{}/{}'.format(day, month, year if year else '2000'), '%d/%b/%Y').date())

    def get_query_tfidf_vector(self, query: str) -> Tuple[pd.Series, str]:
        vector, corrected_query = self.get_query_bow_vector(query)
        return (vector * self.idf_series), corrected_query

    def get_query_bow_vector(self, query: str) -> Tuple[pd.Series, str]:
        result = pd.Series(0., index=self.tfidf_data_frame.index)

        query_tokens = [token.translate(str.maketrans('', '', string.punctuation)).strip()
                        for token in nltk.word_tokenize(query.lower())
                        if token not in self.STOP_WORDS_LIST
                        ]

        spell_checker = SpellChecker()
        unknown_tokens = spell_checker.unknown(query_tokens)
        corrected_tokens = (spell_checker.correction(ut) for ut in unknown_tokens)
        query_length = len(query_tokens)
        for d in (d for d in self._extract_dates(query.lower()) if d in self.tfidf_data_frame.index):
            result.loc[d] += 1 / (query_length if self.normalized else 1)

        for token in query_tokens:
            if token in self.tfidf_data_frame.index:
                result.loc[token] += 1 / (query_length if self.normalized else 1)

        for corrected_token in corrected_tokens:
            if corrected_token in self.tfidf_data_frame.index:
                result.loc[corrected_token] += 1 / (query_length if self.normalized else 1)

        for bigram in ('{} {}'.format(first, second)
                       for first, second in nltk.bigrams(query_tokens) if
                       bool(first) and bool(second)):
            if bigram in self.tfidf_data_frame.index:
                result.loc[bigram] += 1 / (query_length if self.normalized else 1)

        return result, " ".join(corrected_tokens)

    def __str__(self) -> str:
        return str(self.tfidf_data_frame)
