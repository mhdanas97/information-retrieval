import numpy as np

from generalquery import GeneralQuery
from gensimsearchengine import GensimSearchEngine


class GensimQuery(GeneralQuery):
    """
    This class represents an instance of a query,
    initialized every time a query is issued
    """

    def __init__(self, search_engine: GensimSearchEngine, query_text: str):
        super().__init__(search_engine, query_text)

    def get_similarity(self, top: int = 20) -> np.ndarray:
        """
        Get the `top` documents with the highest cosine similarity

        PARAMETERS
        ----------
        top: int number of top documents to return

        RETURNS
        -------
        numpy.ndarray of document numbers
        """
        list_of_files = np.array(sorted([str(i) for i in range(1, 424)]),dtype=int)
        doc2bow_vector = self.search_engine.corpus.dictionary.doc2bow(
            self.search_engine.corpus.preprocess_text(self.query_text))
        tfidf_vector = self.search_engine.tfidf[doc2bow_vector]
        similarities = self.search_engine.similarity_index[tfidf_vector]
        similaritiesgthzero = np.nonzero(similarities)[0]
        return list_of_files[similaritiesgthzero[np.argsort(similarities[similaritiesgthzero])[::-1]][:top]]
