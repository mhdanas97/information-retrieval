from pathlib import Path
from gensim import similarities
from gensim.corpora import textcorpus
from gensim.models import TfidfModel
from preprocessing import get_tokenizer, get_required_token_filters


class GensimSearchEngine:
    """
    This class represents an instance of a search engine,
    initialized at the start of the application
    """

    def __init__(self):
        self.corpus = self._get_corpus()
        self.tfidf = self._get_tfidf_model()
        self.similarity_index = self._get_similarity_index()

    def _get_corpus(self):
        """
        Load saved corpus from disk or rebuild it from documents in directory `corpus`
        Returns
        -------
        Corpus either loaded from disk or rebuilt from documents in directory `corpus`
        """
        path = 'saved_models/corpus.corpus'
        if Path(path).exists():
            return textcorpus.TextDirectoryCorpus.load(path)
        corpus = textcorpus.TextDirectoryCorpus('corpus', tokenizer=get_tokenizer,
                                                token_filters=get_required_token_filters())

        corpus.save(path)
        return corpus

    def _get_tfidf_model(self):
        """
        Load saved TF-IDF model from disk or rebuild based on `self.corpus`
        Returns
        -------
        TF-IDF model either loaded from disk or rebuilt using `TfIdfModel` class
        """
        path = 'saved_models/tfidf-model.model'
        if Path(path).exists():
            return TfidfModel.load(path)
        tfidf_model = TfidfModel(corpus=self.corpus)
        tfidf_model.save(path)
        return tfidf_model

    def _get_similarity_index(self):
        """
        Load saved similarity index from disk or rebuild based on `self.tfidf`
        Returns
        -------
        Similarity index either loaded from disk or rebuilt using `similarities.MatrixSimilarity` class
        """
        path = 'saved_models/similarity-index.index'
        if Path(path).exists():
            return similarities.MatrixSimilarity.load(path)
        index = similarities.MatrixSimilarity(self.tfidf[self.corpus])
        index.save(path)
        return index
