from typing import Tuple

import numpy as np
from scipy.spatial import distance

from generalquery import GeneralQuery
from pandassearchengine import PandasSearchEngine


class PandasQuery(GeneralQuery):
    def __init__(self, search_engine: PandasSearchEngine, query_text: str):
        super().__init__(search_engine, query_text)

    def get_similarity(self, top: int = 20) -> Tuple[np.ndarray, str]:
        tfidf_vector, corrected_query = self.search_engine.get_query_tfidf_vector(self.query_text)
        similarities = (1 - distance.cdist(tfidf_vector.values.reshape(1, -1),
                                           self.search_engine.tfidf_data_frame.values.T, 'cosine')
                        .flatten())
        similaritiesgthzero = np.nonzero(similarities)[0]
        return similaritiesgthzero[np.argsort(similarities[similaritiesgthzero])[::-1]][:top] + 1, corrected_query
