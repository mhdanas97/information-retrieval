import re
from typing import List, Iterable

from gensim.corpora import textcorpus
from gensim import parsing
import nltk

DATES_RE = re.compile(
    r'(?P<month>(?:(?:january)|(?:february)|(?:march)|(?:april)|(?:may)|(?:june)|(?:july)|(?:august)|(?:september)|(?:october)|(?:november)|(?:december))|(?:(?:(?:jan)|(?:feb)|(?:mar)|(?:apr)|(?:may)|(?:jun)|(?:jul)|(?:aug)|(?:sep)|(?:oct)|(?:nov)|(?:dec)|(?:sept))\s*\.))(?:\s+(?P<dayofmonth>\d{1,4}))(?:,\s(?P<year>\d{4}))?')


def _remove_required_stop_words(tokens: Iterable[str]) -> List[str]:
    """Remove the list of stop words in the file `stop-words.txt`
    Parameters
    ----------
    tokens : iterable of str
        Sequence of tokens.

    Returns
    -------
    list of str
        List of tokens without `stopwords`.

    """
    stop_words_list = frozenset(textcorpus.TextCorpus('stop-words.txt',
                                                      token_filters=[]).dictionary.values())
    return textcorpus.remove_stopwords(tokens, stopwords=stop_words_list)


def _get_wordnet_pos(token: str) -> str:
    """Map POS tag to first character lemmatize() accepts"""
    tag = nltk.pos_tag([token])[0][1][0].upper()
    tag_dict = {"J": nltk.corpus.wordnet.ADJ,
                "N": nltk.corpus.wordnet.NOUN,
                "V": nltk.corpus.wordnet.VERB,
                "R": nltk.corpus.wordnet.ADV}

    return tag_dict.get(tag, nltk.corpus.wordnet.NOUN)


def lemmatize_document(tokens: Iterable[str]) -> List[str]:
    """Lemmatize word after getting it's POS Tag
        Parameters
        ----------
        tokens : iterable of str
            Sequence of tokens.

        Returns
        -------
        list of str
            List of lemmatized tokens.

        """

    lemmatizer = nltk.stem.wordnet.WordNetLemmatizer()
    return [lemmatizer.lemmatize(token, _get_wordnet_pos(token)) for token in tokens]


def lemmatize_token(token: str):
    lemmatizer = nltk.stem.wordnet.WordNetLemmatizer()
    return lemmatizer.lemmatize(token, _get_wordnet_pos(token))


def get_tokenizer(text: str) -> List[str]:
    """Tokenize text using custom filters
     Parameters
     ----------
     text: str
        text to be tokenized

    Returns
    -------
    List of str
        Tokenized text after applying filters
     """
    return parsing.preprocessing.preprocess_string(text, filters=[parsing.preprocessing.strip_tags,
                                                                  parsing.preprocessing.strip_punctuation,
                                                                  parsing.preprocessing.strip_multiple_whitespaces,
                                                                  ])


def get_required_token_filters() -> List[callable]:
    """Get the required token filters
    Returns
    -------
    List of callable of the required filters

    """

    return [
        _remove_required_stop_words,
        lemmatize_document
    ]
