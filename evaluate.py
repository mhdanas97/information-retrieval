import re

from gensimquery import GensimQuery
from gensimsearchengine import GensimSearchEngine
from pandasquery import PandasQuery
from pandassearchengine import PandasSearchEngine
import numpy as np


def run_evaluation():
    gensim_query = GensimQuery(GensimSearchEngine(), '')
    # pandas_query = PandasQuery(PandasSearchEngine(), '')
    query_texts = [q.replace('*STOP', '') for q in re.split(r'\*FIND\s+\d+', open('evaluation/Queries.txt').read())[1:]]
    with open('evaluation/gensim_results.txt', mode='w') as g_res:
        # with open('evaluation/pandas_results.txt', mode='w') as p_res:
        for i, q in enumerate(query_texts):
            gensim_query.query_text = q
            # pandas_query.query_text = q
            g_res.write('{}: {}\n'.format(i + 1, str(gensim_query.get_similarity())))
        # p_res.write('{}: {}\n'.format(i + 1, str(pandas_query.get_similarity())))


gold_standard_dict = dict()
with open('evaluation/relevance.txt') as rel:
    for line in rel:
        if bool(line.strip()):
            array = np.fromstring(line, sep=' ', dtype=int)
            gold_standard_dict[array[0]] = array[1:]

# print(gold_standard_dict)

pandas_result_dict = dict()
for i, result_vector in enumerate(re.split(r'\d+:', open('evaluation/gensim_results.txt').read())[1:]):
    pandas_result_dict[i + 1] = np.fromstring(result_vector.strip()[1:-1], sep=' ', dtype=int)
# print(pandas_result_dict)
print(np.array([(v == pandas_result_dict[k][:len(v)]).mean() for k, v in gold_standard_dict.items()]).mean())
