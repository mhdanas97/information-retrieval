import numpy as np


class GeneralQuery:
    def __init__(self, search_engine, query_text: str):
        self.search_engine = search_engine
        self.query_text = query_text

    def get_similarity(self, top: int = 20) -> np.ndarray:
        raise NotImplementedError
