from flask import Flask, jsonify, request

from gensimquery import GensimQuery
from gensimsearchengine import GensimSearchEngine
from pandasquery import PandasQuery
from pandassearchengine import PandasSearchEngine

app = Flask(__name__, static_url_path='/corpus', static_folder='corpus')
gensimQuery = GensimQuery(GensimSearchEngine(), '')
pandasQuery = PandasQuery(PandasSearchEngine(), '')


@app.route('/gensim/', methods=['GET'])
def gensim_retrieve():
    q = request.args.get('q')
    gensimQuery.query_text = q
    return jsonify(documents=gensimQuery.get_similarity().tolist()), 200


@app.route('/pandas/', methods=['GET'])
def pandas_retrieve():
    q = request.args.get('q')
    pandasQuery.query_text = q
    similarity, corrected_query = pandasQuery.get_similarity()
    return jsonify(documents=similarity.tolist(),
                   corrected_query=corrected_query), 200


if __name__ == '__main__':
    app.run()
